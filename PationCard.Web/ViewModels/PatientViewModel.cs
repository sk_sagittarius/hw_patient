﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PationCard.Web.ViewModels
{
    public class PatientViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Iin { get; set; }
    }
}