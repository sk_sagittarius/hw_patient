﻿using PationCard.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PationCard.Web.ViewModels
{
    public class JournalViewModel
    {
        public PatientViewModel Patient { get; set; }
        public int PatientId { get; set; }
        public string Diagnosis { get; set; }
        public DateTime VisitDate { get; set; }
    }
}