﻿using System;

namespace PationCard.Model
{
    public class Journal
    {
        public int Id { get; set; }
        public virtual Patient Patient { get; set; }
        public string Diagnosis { get; set; }
        public DateTime VisitDate { get; set; }
    }
}
