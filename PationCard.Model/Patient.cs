﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PationCard.Model
{
    public class Patient
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Iin { get; set; }
    }
}
